import React from 'react';
import PropTypes from 'prop-types';
import logo from './logo.svg';
import './App.css';
import './bootstrap.min.css'
import { identifier } from '@babel/types';
import { toUnicode } from 'punycode';
import {Link} from 'react-router-dom'


function Book({ title,onClick }) {
  return (
    <div className="answer" onClick={()=>

      onClick(title)
    
    }>
      {title}
    </div>
  )
}
function Hero() {

  return (
    <div className="row">
      <div className="jumbotron col-10 offset-1">
        <h1>
          Author Quizz
         </h1>
      </div>
    </div>
  )
}

function Turn({ author, books,highlight,onAnswerSelected}) {
function highlightToBgColor(highlight){
  const mapping={
    'none':'',
    'correct':'green',
    'wrong':'red'
  }
  return mapping[highlight]

}

  return (
    <div className="row turn" style={{ backgroundColor: highlightToBgColor(highlight) }}>
      <div className="col-4 offset-1">
        <img src={author.imageUrl} className="authorimage" alt="Author"></img>
      </div>
      <div className="col-6">
        {books.map((title) =>
          <Book title={title} key={title} onClick={onAnswerSelected}></Book>
        )}
      </div>

    </div>
  )

}
Turn.protoTypes={
  author:PropTypes.shape({
    name:PropTypes.string.isRequired,
    imageUrl:PropTypes.string.isRequired,
    imageSource:PropTypes.string.isRequired,
    books:PropTypes.arrayOf(PropTypes.string).isRequired
  }),
  books:PropTypes.arrayOf(PropTypes.string).isRequired,
  onAnswerSelected:PropTypes.func.isRequired,
  highlight:PropTypes.string.isRequired

}

function Continue() {
  return (<div>

  </div>)
}

function Footer() {
  return (
    <div id="footer" className="row">
      <div className="col-12" style={{ textAlign: "center" }}>
        <p className="text-muted credit">
          All images all from @copyright
       </p>

      </div>

    </div>
  )
}


function App({ turnData,highlight,onAnswerSelected }) {
  return (
    <div className="container-fluid">
      <Hero></Hero>
      <Turn {...turnData} highlight={highlight} onAnswerSelected={onAnswerSelected}></Turn>
      <Continue> </Continue>
      <p><Link to="/add">Add an author</Link></p>
      <Footer></Footer>
    </div>

  );
}

export default App;
