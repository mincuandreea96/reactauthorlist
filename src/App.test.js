import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Enzyme, { mount, shallow, render } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { exportAllDeclaration } from '@babel/types';

Enzyme.configure({
  adapter: new Adapter()
})

const state = {
  turnData: {
    books: ['the shining', 'it'],
    author: {
      name: 'name is',
      imageUrl: 'images/logo192.png',
      imageSource: 'wiki',
      books: ['david book', 'a tale to the mister']
    },
  },
  highlight: 'none'
}

describe("authoor quiz", () => {


  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App {...state} onAnswerSelected={() => {

    }} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
   
  describe("when no answer has been selected",()=>{
    let wapper;
    beforeAll(()=>{
         wapper=mount(<App {...state} onAnswerSelected={()=>{

         }} ></App>)
    });
   
    it("shoud have no background color",()=>{
      expect(wapper.find("div.row.turn").props().style.backgroundColor).toBe('')
    })

  });


  describe("when correct answer has been selected",()=>{
    let wapper;
    beforeAll(()=>{
         wapper=mount(<App{...(Object.assign({},state,{highlight:'correct'}))} onAnswerSelected={()=>{

         }} ></App>)
    });
   
    it("shoud have a green background color",()=>{
      expect(wapper.find("div.row.turn").props().style.backgroundColor).toBe('green')
    })

  });

  describe("when wrong answer has been selected",()=>{
    let wapper;
    beforeAll(()=>{
         wapper=mount(<App{...(Object.assign({},state,{highlight:'wrong'}))} onAnswerSelected={()=>{

         }} ></App>)
    });
   
    it("shoud have a red background color",()=>{
      expect(wapper.find("div.row.turn").props().style.backgroundColor).toBe('red')
    })

  });


  describe("when the first answer has been selected",()=>{
    let wapper;
    const handleAnswerSelected=jest.fn();

    beforeAll(()=>{
         wapper=mount(<App {...state} onAnswerSelected={handleAnswerSelected} ></App>)
         wapper.find('.answer').first().simulate('click');

    });
   
    it("onAnswerSelected should be called",()=>{
      expect(handleAnswerSelected).toHaveBeenCalled();
    })
    it("selected should be the shining",()=>{
      expect(handleAnswerSelected).toHaveBeenCalledWith("the shining");
    })

   
  });




});