import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { shuffle, sample } from 'underscore'
import { BrowserRouter, Route } from 'react-router-dom'

const authors = [
    {
        name: "forica",
        imageUrl: 'images/logo192.png',
        imageSource: "from wikipedia",
        books: ['The add offf me', 'another book']
    },
    {
        name: "diana",
        imageUrl: 'images/logo192.png',
        imageSource: "from wikipedia",
        books: ['The add ofdd me', 'another book']
    },
    {
        name: "andreea",
        imageUrl: 'images/logo192.png',
        imageSource: "from wikipedia",
        books: ['The add of me', 'another book']
    }
];
function getTurnData(authors) {
    const allBooks = authors.reduce(function (p, c, i) {
        return p.concat(c.books)
    }, [])

    const fourRandomBooks = shuffle(allBooks).slice(0, 4)
    const answer = sample(fourRandomBooks)

    return {
        books: fourRandomBooks,
        author: authors.find((author) =>
            author.books.some((title) =>
                title === answer)
        )
    }

}

const state = {
    turnData: getTurnData(authors),
    highlight: ''
}

function onAnswerSelected(answer) {
    const isCorrect = state.turnData.author.books.some((book) =>
        book === answer)
    state.highlight = isCorrect ? 'correct' : 'wrong'
    render();
}

function AuthorForm(match) {
    return (
        <div>
            Add an author
            <p>{JSON.stringify(match)}</p>
        </div>
    )
}
function Application() {
    return (<App {...state} onAnswerSelected={onAnswerSelected} />)
}


function render() {
    ReactDOM.render(<BrowserRouter>
        <React.Fragment>
            <Route exact path="/" component={Application} />
            <Route path="/add" component={AuthorForm} />
        </React.Fragment>
    </BrowserRouter>, document.getElementById('root'));

}
render();
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
